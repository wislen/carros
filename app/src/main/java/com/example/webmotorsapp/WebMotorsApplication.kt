package com.example.webmotorsapp

import android.app.Application
import com.example.webmotorsapp.di.dataSourceModule
import com.example.webmotorsapp.di.repositoryModule
import com.example.webmotorsapp.di.retrofitModule
import com.example.webmotorsapp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class WebMotorsApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@WebMotorsApplication)
            modules(
                listOf(
                    retrofitModule,
                    repositoryModule,
                    viewModelModule,
                    dataSourceModule
                )
            )
        }
    }


}