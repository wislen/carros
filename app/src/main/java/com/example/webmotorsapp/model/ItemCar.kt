package com.example.webmotorsapp.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemCar(
    @SerializedName("ID") val iD : Int,
    @SerializedName("Make") val make : String,
    @SerializedName("Model") val model : String,
    @SerializedName("Version") val version : String,
    @SerializedName("Image") val image : String,
    @SerializedName("KM") val kM : Int,
    @SerializedName("Price") val price : String,
    @SerializedName("YearModel") val yearModel : Int,
    @SerializedName("YearFab") val yearFab : Int,
    @SerializedName("Color") val color : String
) : Parcelable