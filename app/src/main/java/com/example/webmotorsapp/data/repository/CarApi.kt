package com.example.webmotorsapp.data.repository

import com.example.webmotorsapp.common.Constants.GET_VEHICLES
import com.example.webmotorsapp.model.Cars
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CarApi {

    @GET(GET_VEHICLES)
    fun getListCar(@Query("Page") page: Int): Deferred<Response<Cars>>
}