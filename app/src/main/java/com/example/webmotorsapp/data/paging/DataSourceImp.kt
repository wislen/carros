package com.example.webmotorsapp.data.paging

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.PageKeyedDataSource
import com.example.webmotorsapp.common.Constants.UNEXPECTED_ERROR_MESSAGE
import com.example.webmotorsapp.common.NetworkState
import com.example.webmotorsapp.common.base.BaseViewModel
import com.example.webmotorsapp.data.repository.CarApi
import com.example.webmotorsapp.data.repository.ResponseResult
import com.example.webmotorsapp.model.ItemCar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DataSourceImp(
    private val api: CarApi
) : BaseViewModel(), IDataSource {

    override fun createObservable(
        requestPage: Int,
        nextPage: Int,
        requestLoadSize: Int,
        loadInitialCallback: PageKeyedDataSource.LoadInitialCallback<Int, ItemCar>?,
        loadCallback: PageKeyedDataSource.LoadCallback<Int, ItemCar>?,
        network: MutableLiveData<Pair<NetworkState, String>>,
        moreCar: Boolean
    ) {
        if (moreCar) {
            network.postValue(Pair(NetworkState.LOADING_MORE, ""))
        } else {
            network.postValue(Pair(NetworkState.LOADING, ""))
        }
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                getResult {
                    api.getListCar(requestPage * requestLoadSize).await()
                }
            }.let {
                withContext(Dispatchers.Main) {
                    when (it) {
                        is ResponseResult.Success -> {
                            it.result.data?.let { cars ->
                                loadInitialCallback?.onResult(
                                    cars,
                                    null,
                                    nextPage
                                )
                                loadCallback?.onResult(
                                    cars,
                                    nextPage
                                )
                            }
                            network.postValue(Pair(NetworkState.SUCCESS, ""))
                        }

                        is Error -> {
                            network.postValue(Pair(NetworkState.ERROR, it.message.toString()))
                        }

                        else -> {
                            network.postValue(
                                Pair(
                                    NetworkState.UNEXPECTED_ERROR,
                                    UNEXPECTED_ERROR_MESSAGE
                                )
                            )
                        }
                    }
                }
            }
        }
    }
}