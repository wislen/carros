package com.example.webmotorsapp.data.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.example.webmotorsapp.common.NetworkState
import com.example.webmotorsapp.model.ItemCar

class CarDataSouce(
    private val iDataSource: IDataSource
) : PageKeyedDataSource<Int, ItemCar>() {

    val callBackNetwork = MutableLiveData<Pair<NetworkState, String>>()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, ItemCar>
    ) {
        iDataSource.createObservable(
            1,
            2,
            params.requestedLoadSize,
            callback,
            null,
            callBackNetwork
        )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, ItemCar>) {
        val page = params.key
        iDataSource.createObservable(
            page,
            page.inc(),
            params.requestedLoadSize,
            null,
            callback,
            callBackNetwork
            , true
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, ItemCar>) {
        val page = params.key
        iDataSource.createObservable(
            page,
            page.dec(),
            params.requestedLoadSize,
            null,
            callback,
            callBackNetwork
        )
    }
}