package com.example.webmotorsapp.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations.switchMap
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.webmotorsapp.common.Constants.INITIAL_PAGE
import com.example.webmotorsapp.common.Constants.NUMBER_CHARGE_CAR
import com.example.webmotorsapp.common.NetworkState
import com.example.webmotorsapp.data.paging.CarDataSouceFactory
import com.example.webmotorsapp.model.ItemCar

class CarRepositoryImp(private val carDataSouceFactory: CarDataSouceFactory) : CarRepository {

    override fun getListCar(): LiveData<PagedList<ItemCar>> {

        val config = PagedList.Config.Builder()
            .setPageSize(INITIAL_PAGE)
            .setInitialLoadSizeHint(INITIAL_PAGE)
            .setPrefetchDistance(NUMBER_CHARGE_CAR)
            .setEnablePlaceholders(false)
            .build()

        return LivePagedListBuilder(carDataSouceFactory, config).build()
    }

    override fun getStatusNetWork(): LiveData<Pair<NetworkState, String>> {
        return switchMap(carDataSouceFactory.source) { it.callBackNetwork }
    }
}