package com.example.webmotorsapp.data.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.example.webmotorsapp.common.NetworkState
import com.example.webmotorsapp.model.ItemCar

interface IDataSource {

    fun createObservable(
        requestPage: Int,
        nextPage: Int,
        requestLoadSize: Int,
        loadInitialCallback: PageKeyedDataSource.LoadInitialCallback<Int, ItemCar>?,
        loadCallback: PageKeyedDataSource.LoadCallback<Int, ItemCar>?,
        network: MutableLiveData<Pair<NetworkState, String>>,
        moreCar: Boolean = false
    )
}