package com.example.webmotorsapp.data.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.webmotorsapp.model.ItemCar

class CarDataSouceFactory(
    private val carDataSource: CarDataSouce
) : DataSource.Factory<Int, ItemCar>() {

    val source = MutableLiveData<CarDataSouce>()

    override fun create(): DataSource<Int, ItemCar> {
        source.postValue(carDataSource)
        return carDataSource
    }
}