package com.example.webmotorsapp.data.repository

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.example.webmotorsapp.common.NetworkState
import com.example.webmotorsapp.model.ItemCar

interface CarRepository {
    fun getListCar(): LiveData<PagedList<ItemCar>>
    fun getStatusNetWork(): LiveData<Pair<NetworkState,String>>
}