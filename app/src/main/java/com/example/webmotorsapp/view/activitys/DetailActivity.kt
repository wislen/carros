package com.example.webmotorsapp.view.activitys

import android.os.Bundle
import androidx.core.text.HtmlCompat
import coil.api.load
import com.example.webmotorsapp.R
import com.example.webmotorsapp.common.Constants.CAR
import com.example.webmotorsapp.common.base.BaseActivity
import com.example.webmotorsapp.extension.extras
import com.example.webmotorsapp.model.ItemCar
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : BaseActivity() {

    private val car: ItemCar by extras(CAR)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        configureActionBar()
        setValuesCar()
    }

    private fun setValuesCar() {
        ivCar.load(car.image)
        tvVersion.text = car.version
        tvColor.text = car.color
        tvModelMake.text = HtmlCompat.fromHtml(
            getString(
                R.string.make_model,
                car.make,
                car.model
            ),
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )
        tvKM.text = getString(R.string.km, car.kM)
        tvPrice.text = getString(R.string.money, car.price)
        tvYear.text =
            getString(R.string.year_fab_model, car.yearFab, car.yearModel)
    }

    private fun configureActionBar() {
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle(R.string.detail_car)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}
