package com.example.webmotorsapp.view.activitys

import android.os.Bundle
import android.view.View
import androidx.paging.DataSource
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.webmotorsapp.R
import com.example.webmotorsapp.common.Constants.CAR
import com.example.webmotorsapp.common.Constants.STATE_LOADING
import com.example.webmotorsapp.common.Constants.STATE_SUCCESS
import com.example.webmotorsapp.common.NetworkState.Companion.ERROR
import com.example.webmotorsapp.common.NetworkState.Companion.LOADING
import com.example.webmotorsapp.common.NetworkState.Companion.LOADING_MORE
import com.example.webmotorsapp.common.NetworkState.Companion.SUCCESS
import com.example.webmotorsapp.common.NetworkState.Companion.UNEXPECTED_ERROR
import com.example.webmotorsapp.common.base.BaseActivity
import com.example.webmotorsapp.extension.dialog
import com.example.webmotorsapp.extension.newIntent
import com.example.webmotorsapp.model.ItemCar
import com.example.webmotorsapp.view.adapters.CarAdapter
import com.example.webmotorsapp.viewmodel.CarViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity() {

    private lateinit var dataSource: DataSource<*, ItemCar>
    private val viewModel by viewModel<CarViewModel>()
    private val adapter: CarAdapter by lazy {
        CarAdapter(onClickListener = { _, car ->
            callBackItemCarAdapter(
                car
            )
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkConection { subscribeToList() }
        startListComponents()
    }

    private fun startListComponents() {
        rvCar.let {
            val layoutManager = LinearLayoutManager(this)
            it.layoutManager = layoutManager
            it.isNestedScrollingEnabled = false
            it.adapter = adapter
        }
    }

    private fun subscribeToList() {
        viewModel.getListCar().onResult(this@MainActivity) {
            dataSource = it.dataSource
            adapter.submitList(it)
        }

        viewModel.getStatusNetWork().onResult(this@MainActivity) {
            when (it.first) {

                LOADING_MORE -> {
                    setVisibilityProgressPaging(true)
                }

                LOADING -> {
                    vfDashboardContent.displayedChild = STATE_LOADING
                }

                SUCCESS -> {
                    vfDashboardContent.displayedChild = STATE_SUCCESS
                    setVisibilityProgressPaging(false)
                }

                ERROR -> {
                    this.dialog(
                        title = R.string.title_alert_error,
                        body = it.second,
                        positiveText = R.string.positive_alert_error,
                        positiveBlock = { dataSource.invalidate() }
                    )
                }
                UNEXPECTED_ERROR -> {
                    this.dialog(
                        title = R.string.title_alert_error,
                        body = R.string.body_alert_error,
                        positiveText = R.string.positive_alert_error,
                        positiveBlock = { dataSource.invalidate() }
                    )
                }
            }
        }
    }

    private fun callBackItemCarAdapter(car: ItemCar) {
        val bundle = Bundle().apply {
            putParcelable(CAR, car)
        }
        newIntent(DetailActivity::class.java, bundle)
        overridePendingTransition(R.anim.slide_out_bottom, R.anim.slide_in_bottom)
    }

    private fun setVisibilityProgressPaging(visible: Boolean) {
        progress_paging.visibility = if (visible) View.VISIBLE else View.GONE
    }
}
