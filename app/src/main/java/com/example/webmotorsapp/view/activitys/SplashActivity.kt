package com.example.webmotorsapp.view.activitys

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.webmotorsapp.R
import com.example.webmotorsapp.extension.newIntent

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed({
            newIntent(MainActivity::class.java)
            finish()
        }, 5000)
    }
}
