package com.example.webmotorsapp.view.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.example.webmotorsapp.R
import com.example.webmotorsapp.model.ItemCar
import kotlinx.android.synthetic.main.item_car.view.*

class CarAdapter(private val onClickListener: (View, ItemCar) -> Unit) :
    PagedListAdapter<ItemCar, CarAdapter.ViewHolder>(carDiff) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_car, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let {
            holder.itemView.setOnClickListener { view ->
                onClickListener.invoke(view, it)
            }
            holder.bind(it)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(item: ItemCar) {
            with(itemView) {
                ivCar.load(item.image) {
                    crossfade(true)
                    placeholder(R.drawable.bg_shimmer)
                }
                tvVersion.text = item.version
                tvColor.text = item.color
                tvModelMake.text = HtmlCompat.fromHtml(
                    context.getString(
                        R.string.make_model,
                        item.make,
                        item.model
                    ),
                    HtmlCompat.FROM_HTML_MODE_LEGACY
                )
                tvKM.text = context.getString(R.string.km, item.kM)
                tvPrice.text = context.getString(R.string.money, item.price)
                tvYear.text =
                    context.getString(R.string.year_fab_model, item.yearFab, item.yearModel)
            }
        }
    }

    companion object {
        val carDiff = object : DiffUtil.ItemCallback<ItemCar>() {
            override fun areItemsTheSame(oldItem: ItemCar, newItem: ItemCar): Boolean {
                return oldItem.iD == newItem.iD
            }

            override fun areContentsTheSame(oldItem: ItemCar, newItem: ItemCar): Boolean {
                return oldItem == newItem
            }

        }
    }
}