package com.example.webmotorsapp.extension

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog

fun Context.dialog(
    title: Int,
    body: String,
    positiveText: Int,
    positiveBlock: () -> Unit,
    negativeText: Int? = null,
    negativeBlock: () -> Unit = {}
) {
    val alertDialog = AlertDialog.Builder(this).create()
    alertDialog.setTitle(title)
    alertDialog.setMessage(body)
    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(positiveText)) { _, _ ->
        positiveBlock()
    }
    negativeText?.let {
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(it)) { _, _ ->
            negativeBlock()
        }
    }
    alertDialog.show()
}

fun Context.dialog(
    title: Int,
    body: Int,
    positiveText: Int,
    positiveBlock: () -> Unit,
    negativeText: Int? = null,
    negativeBlock: () -> Unit = {}
) {
    val alertDialog = AlertDialog.Builder(this).create()
    alertDialog.setTitle(title)
    alertDialog.setMessage(getString(body))
    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(positiveText)) { _, _ ->
        positiveBlock()
    }
    negativeText?.let {
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(it)) { _, _ ->
            negativeBlock()
        }
    }
    alertDialog.show()
}


fun <T> Context.newIntent(classToOpen: Class<T>, bundle: Bundle? = null) {
    val intent = Intent(this, classToOpen)
    bundle?.let {
        intent.putExtras(it)
    }
    startActivity(intent)
}

inline fun <reified T> Activity.extras(key: String): Lazy<T>? = lazy {
    val value = intent.extras?.get(key)
    if (value is T) {
        value
    } else {
        throw Throwable()
    }
}
