package com.example.webmotorsapp.di

import com.example.webmotorsapp.data.paging.CarDataSouce
import com.example.webmotorsapp.data.paging.DataSourceImp
import com.example.webmotorsapp.data.paging.IDataSource
import org.koin.dsl.module

val dataSourceModule = module {
    single { DataSourceImp(get()) as IDataSource }
    single { CarDataSouce(get()) }
}