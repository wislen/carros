package com.example.webmotorsapp.di

import com.example.webmotorsapp.data.repository.CarRepository
import com.example.webmotorsapp.data.repository.CarRepositoryImp
import com.example.webmotorsapp.data.paging.CarDataSouce
import com.example.webmotorsapp.data.paging.CarDataSouceFactory
import org.koin.dsl.module

val repositoryModule = module {
    single { CarRepositoryImp(get()) as CarRepository }
    single { CarDataSouceFactory(get() as CarDataSouce) }
}