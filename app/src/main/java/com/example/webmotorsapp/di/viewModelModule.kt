package com.example.webmotorsapp.di


import com.example.webmotorsapp.viewmodel.CarViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { CarViewModel(get()) }
}