package com.example.webmotorsapp.common

object Constants {

    const val BASE_URL = "http://desafioonline.webmotors.com.br/"
    const val GET_VEHICLES = "api/OnlineChallenge/Vehicles"
    const val COD_500 = 500
    const val INTERNAL_SERVER_MESSAGE = "Serviço indisponível, tente novamente mais tarde."
    const val UNEXPECTED_ERROR_MESSAGE = "Erro inesperado"
    const val LOG = "WEBMOTORS"
    const val ERROR = "ERROR:"
    const val CAR = "CAR"

    const val NUMBER_CHARGE_CAR = 10
    const val INITIAL_PAGE = 1

    const val STATE_SUCCESS = 1
    const val STATE_LOADING = 0
}