package com.example.webmotorsapp.common.base

import android.util.Log
import androidx.lifecycle.ViewModel
import com.example.webmotorsapp.common.Constants.COD_500
import com.example.webmotorsapp.common.Constants.INTERNAL_SERVER_MESSAGE
import com.example.webmotorsapp.common.Constants.LOG
import com.example.webmotorsapp.data.repository.ResponseResult
import com.example.webmotorsapp.data.repository.ResponseWrapper
import retrofit2.Response


abstract class BaseViewModel : ViewModel() {

    protected suspend fun <T> getResult(call: suspend () -> Response<T>): ResponseResult<ResponseWrapper<T>> {
        try {
            val response = call()
            if (response.isSuccessful) {
                val body = response.body()
                if (null != body) return ResponseResult.Success(ResponseWrapper(body, null))
            }
            if (response.code() == COD_500) {
                return error(INTERNAL_SERVER_MESSAGE)
            }
            return error("${response.code()} ${response.message()}")
        } catch (e: Exception) {
            return error(e.message ?: e.toString())
        }
    }

    private fun <T> error(msg: String): ResponseResult<ResponseWrapper<T>> {
        Log.e(LOG, msg)
        return ResponseResult.Error(ResponseWrapper(null, msg))
    }


}
