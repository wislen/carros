package com.example.webmotorsapp.common.base

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.observe
import com.example.webmotorsapp.R
import com.example.webmotorsapp.extension.dialog

abstract class BaseActivity : AppCompatActivity() {

    protected fun checkConection(action: () -> Unit) {
        if (!checkNetworkState()) {
            this.dialog(
                title = R.string.title_alert_not_conection,
                body = R.string.body_alert_not_conection,
                positiveText = R.string.positive_alert_not_conection,
                positiveBlock = { checkConection { } }
            )
        } else {
            action.invoke()
        }
    }

    protected fun <T> LiveData<T>.onResult(owner: LifecycleOwner, action: (T) -> Unit) {
        observe(owner) { data ->
            data?.let(action)
        }
    }

    override fun onBackPressed() {
        overridePendingTransition(R.anim.slide_out_bottom, R.anim.slide_in_bottom)
        super.onBackPressed()
    }

    private fun checkNetworkState(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val nw = connectivityManager.activeNetwork ?: return false
            val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
            return when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            val nwInfo = connectivityManager.activeNetworkInfo ?: return false
            return nwInfo.isConnected
        }
    }
}