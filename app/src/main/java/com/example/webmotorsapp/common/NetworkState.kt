package com.example.webmotorsapp.common

data class NetworkState constructor(
    val status: Status
) {

    companion object {
        val SUCCESS = NetworkState(Status.SUCCESS)
        val LOADING = NetworkState(Status.LOADING)
        val LOADING_MORE = NetworkState(Status.LOADING_MORE)
        val ERROR = NetworkState(Status.FAILED)
        val UNEXPECTED_ERROR = NetworkState(Status.UNEXPECTED_ERROR)
        val FINISH = NetworkState(Status.FINISH)
    }
}

enum class Status() {
    LOADING,
    SUCCESS,
    FAILED,
    UNEXPECTED_ERROR,
    LOADING_MORE,
    FINISH
}