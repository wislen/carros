package com.example.webmotorsapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.webmotorsapp.common.NetworkState
import com.example.webmotorsapp.common.base.BaseViewModel
import com.example.webmotorsapp.data.repository.CarRepository

class CarViewModel(private val carRepository: CarRepository) : BaseViewModel() {

    fun getListCar() = carRepository.getListCar()

    fun getStatusNetWork() = carRepository.getStatusNetWork()
}